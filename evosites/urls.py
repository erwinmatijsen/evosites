# URL stuff
from django.conf.urls import url, include

# Static
from django.conf import settings
from django.conf.urls.static import static

# Views
from content.views import blog_single

# Helpers
from evosites.helpers import get_url, get_contact_url, get_blog_url

# Entries
from evosites.config import ENTRIES


# User defined pages
exclude_entries = ['HOME', 'BLOG', 'CONTACT']
pages = []
for item in ENTRIES:
    if item not in exclude_entries:
        pages.append(get_url(item))

urlpatterns = [
    # Home
    get_url('HOME'),

    # Blog yearly archive
    get_url('BLOG'),
    get_blog_url(),

    # Single Posts
    url(r'^blog/(?P<slug>[-\w]+)$', blog_single, name="blog-single"),

    # Contact
    get_url('CONTACT'),
    get_contact_url(),

] + pages + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)