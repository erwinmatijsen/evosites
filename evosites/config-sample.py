import os

TEMPLATE = 'demo'  # Must be folder name in /templates/
SITE_NAME = "My Site"
SITE_TAG = "My Cool Site"

"""
Contenful

Get your Space ID and API key from https://contentful.com

See https://www.contentful.com/r/knowledgebase/contentful-101/ to get started with Contentful and create a Space.
See for API keys: https://www.contentful.com/developers/docs/references/content-management-api/#/reference/api-keys  

"""

CONTENTFUL_SPACE_ID = 'your-space-id'
CONTENTFUL_DELIVERY_KEY = 'your-api-key'


"""
Production settings

"""
ENV = 'Dev'  # or Prod or Testing.
HOST_TESTING = ['test.example.com', ]  # Enter one or more of your own allowed domains for testing
HOST_PROD = ['example.com', 'www.example.com', ]  # Enter one or more of your own allowd domains for production
SECRET_KEY_TESTING = os.environ['DJANGO_SECRET']
SECRET_KEY_PROD = os.environ['DJANGO_SECRET']

# Static en media files will be collected in the folders you set here. Best keep this out of your main website and
# serve them with another server (nginx for example)
STATIC_ROOT_TESTING = '/home/yourname/static/'  # Full path
MEDIA_ROOT_TESTING = '/home/yourname/media/'  # Full path
STATIC_ROOT_PROD = '/home/yourname/media/'  # Full path
MEDIA_ROOT_PROD = '/home/yourname/media/'  # Full path


"""
ENTRIES

Here you define which pages should appear in the menu, and how.

There are three special pages which should always be in ENTRIES:
- HOME
- BLOG
- CONTACT

Next to those thee pages, you can define your own. Don't forget to create the corresponding template!

Options which every Entrie should have:
- IN_MENU can be eiterh 'True' or 'False'
- NAME defines how the page should appear in the menu (e.g. for Blog you can set 'Archive')
- URL defines to which URL pages should point (Only use letters, numbers, underscores or hyphens)
- ORDER determines in which order pages are shown in the menu

Options regarding HOME and BLOG:
Define if your Homepage should be a regular page, or the latest entrie(s) from your blog.
- HOME_PAGE_TYPE should be 'Latest' or 'Page'

Define how many posts should appear on the Blogpage (latest entries) and if the latest entries
should show an summary or the full text.
- BLOG_LATEST_NUMBER should be an positive round value, e.g. 3. Set to 0 (zero) for no limits
- BLOG_FULL_OR_SUMMARY should be 'Summary' or 'Full'
- SUMMARY_MAX_LENGTH sets the maximum number of characters (not words). Should be an positive round value, e.g. 250.

"""

ENTRIES = {
    # IN_MENU: 'True' or 'False'
    # NAME: set an name for your page
    # URL: set an url ('' for root)
    # ORDER: a number

    # Special cases:
    # HOME_PAGE_TYPE: 'Latest' or 'Page'
    # BLOG_LATEST_NUMBER: positive, round number. 0 (zero) for no limit
    # BLOG_FULL_OR_SUMMARY: 'Summary' or 'Full'
    # SUMMARY_MAX_LENGTH: Max number of characters (not words)

    'HOME':
        {
            'IN_MENU': 'True',
            'NAME': 'Home',
            'URL': '',
            'ORDER': 1,
            'HOME_PAGE_TYPE': 'Latest',
        },
    'BLOG':
        {
            'IN_MENU': 'True',
            'NAME': 'Archief',
            'URL': 'blog',
            'ORDER': 2,
            'BLOG_LATEST_NUMBER': 1,
            'BLOG_FULL_OR_SUMMARY': 'Summary',
            'SUMMARY_MAX_LENGTH': 500,
        },
    'CONTACT':
        {
            'IN_MENU': 'True',
            'NAME': 'Contact',
            'URL': 'contact',
            'ORDER': 4,
        },
    'EXAMPLE':
        {
            'IN_MENU': 'True',
            'NAME': 'Example',
            'URL': 'sample',
            'ORDER': 3,
        },
}


"""
URL'S

Set how the URL's for your archive and contactpage will appear

Archive example: /blog/year
Contact Thank You page example: /contact/thanks

"""
ARCHIVE_URL = 'year'
CONTACT_THANKS_URL = 'thanks'


"""
LANGUAGE SETTINGS

Set your locale, timezone, dateformat
"""

LANGUAGE_CODE = 'nl-NL'
TIME_ZONE = 'Europe/Amsterdam'  # https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
DATE_FORMAT = 'd-m-Y'
DATE_INPUT_FORMATS = ['%d-%m-%Y', ]
DECIMAL_SEPARATOR = ','


"""
E-MAIL

E-mail settings
The project uses Anymail (https://github.com/anymail/django-anymail)
See https://anymail.readthedocs.io/en/stable/esps/ for a list of support Email Server Providers (ESP's)
"""

CONTACT_EMAIL = 'yourname@example.com'
ANYMAIL = {
    "MAILGUN_API_KEY": os.environ['MAILGUN_API'],
    "MAILGUN_SENDER_DOMAIN": 'mg.example.com',
}
EMAIL_BACKEND = "anymail.backends.mailgun.EmailBackend"