"""
Set of helper functions to determine and validate the url for the pages.
In evosites.urls 'get_url()' is run, which does some validation and cleaning up and
calls 'set_view_and_name()' to determine what view and name to set.
"""


def set_view_and_name(menu_item):
    """
    Helper function to return the view and name to use in the url.
    HOME, BLOG and CONTACT are special cases.

    :param menu_item:
    :return: set_name, set_view
    """
    from content.views import blog_archive, ContactView
    from django.views.generic import TemplateView
    from evosites.config import ENTRIES

    if menu_item == 'HOME':
        set_name = 'home'
        set_view = get_home_view()
        return set_name, set_view
    elif menu_item == 'BLOG':
        set_name = 'blog-archive-current-year'
        set_view = blog_archive
        return set_name, set_view
    elif menu_item == 'CONTACT':
        set_name = 'contact'
        set_view = ContactView.as_view()
        return set_name, set_view
    else:
        # ToDO: slugify set_name
        set_name = menu_item.lower()

        url = ENTRIES[menu_item]['URL']
        url = url[:-1]
        set_view = TemplateView.as_view(template_name='pages/{}.html'.format(url))

        return set_name, set_view


def get_home_view():
    """
    Helper functie to return the view for the homepage (in evosites.urls).
    User has to set 'HOME_PAGE_TYPE' to 'Page' or 'Latest' in evosites.config

    :return: home_view
    """
    from evosites.config import ENTRIES
    from content.views import blog_latest, HomeView

    if ENTRIES['HOME']['HOME_PAGE_TYPE'] == 'Latest':
        home_view = blog_latest
        return home_view
    elif ENTRIES['HOME']['HOME_PAGE_TYPE'] == 'Page':
        home_view = HomeView.as_view()
        return home_view
    else:
        raise ValueError("Config: 'HOME_PAGE_TYPE' is not set to 'Latest' or 'Page'.")


def get_url(menu_item):
    from evosites.config import ENTRIES
    from django.core.validators import slug_re
    from django.conf.urls import url

    item = ENTRIES[menu_item]

    # First check if url is valid, clean up
    if item['URL'] != '':
        # Remove leading and trailing forward slashes
        if item['URL'].startswith('/'):
            item['URL'] = item['URL'][1:]
        if item['URL'].endswith('/'):
            item['URL'] = item['URL'][:-1]

        # Split the url to sanitize slugs
        slugs = item['URL'].split('/')
        for slug in slugs:
            if not slug_re.match(slug):
                raise ValueError(("Config: '{}: URL' contains invalid characters. "
                                  "Only use letters, numbers, underscores, hyphens or forward slashes"
                                  .format(menu_item)))
            else:
                # Rebuild url, add trailing slash (only needed when not '')
                item['URL'] = '/'.join(slugs) + '/'

    # URL seems good, build regex, get name and view
    regex = r'^{}$'.format(item['URL'])
    set_name, set_view = set_view_and_name(menu_item)

    # Return url
    return url(regex, set_view, name=set_name)


def get_contact_url():
    from evosites.config import CONTACT_THANKS_URL
    from content.views import ContactBedanktView
    from django.conf.urls import url

    regex = r'^contact/{}/$'.format(CONTACT_THANKS_URL)

    return url(regex, ContactBedanktView.as_view(), name="thanks")


def get_blog_url():
    from evosites.config import ARCHIVE_URL
    from content.views import blog_archive
    from django.conf.urls import url

    regex = r'^blog/{}/(?P<year>[0-9]{{4}})/$'.format(ARCHIVE_URL)

    return url(regex, blog_archive, name="blog-archive")