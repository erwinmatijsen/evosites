from django import template
from evosites.config import ENTRIES

register = template.Library()


@register.inclusion_tag('menu/main.html')
def menu_walker():
    """
    Check if constants regarding menu are set in config and
    make them available for the template.

    """
    from evosites.helpers import set_view_and_name

    menu_items = []
    for key, value in ENTRIES.items():

        if not value['IN_MENU'] == 'True' and not value['IN_MENU'] == 'False':
            raise ValueError("Config: '{}: IN_MENU' is not set to 'True' or 'False'.".format(key))

        if value['IN_MENU'] == 'True':
            set_name, set_view = set_view_and_name(key)
            menu_items.append([set_name, value['NAME'], value['ORDER']])

            # Sort by value['ORDER']
            def getKey(item):
                return item[2]
            menu_items = sorted(menu_items, key=getKey)

    #home_menu = ENTRIES['HOME']['IN_MENU']
    #home_url = ENTRIES['HOME']['URL']
    #home_menu_name = ENTRIES['HOME']['NAME']

    #blog_menu = ENTRIES['BLOG']['IN_MENU']
    #blog_url = ENTRIES['BLOG']['URL']
    #blog_menu_name = ENTRIES['BLOG']['NAME']

    return {
        'menu_items': menu_items,
        #'home_menu': home_menu,
        #'home_menu_url': home_url,
        #'home_menu_name': home_menu_name,
        #'blog_menu': blog_menu,
        #'blog_url': blog_url,
        #'blog_name': blog_menu_name
    }