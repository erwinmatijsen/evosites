from django import template
from evosites.config import SITE_NAME, SITE_TAG

register = template.Library()


@register.simple_tag()
def site_information():
    """
    Makes the sitename and sitetag (set in config) available througout the template
    """
    site_info = {'site_name': SITE_NAME,
                 'site_tag': SITE_TAG,
                 }
    return site_info
