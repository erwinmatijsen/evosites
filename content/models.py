# Third party
import contentful

# Python
import os

# Evosites
from evosites.config import CONTENTFUL_SPACE_ID, CONTENTFUL_DELIVERY_KEY

# Contentfull
client = contentful.Client(
    os.environ.get('CTF_SPACE_ID', CONTENTFUL_SPACE_ID),
    os.environ.get('CTF_DELIVERY_KEY', CONTENTFUL_DELIVERY_KEY)
)
