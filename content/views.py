# Class Based Views
from django.views.generic import TemplateView, FormView

# Other Django
from django.shortcuts import render
from django.http import Http404
from django.core.urlresolvers import reverse_lazy
from django.views.decorators.cache import cache_control

# Evosites
from .forms import ContactForm
from .models import client
from evosites.config import ENTRIES

# Python
from datetime import date, datetime

#######################################################################################################################


class HomeView(TemplateView):
    """
    Renders the homepage (if HOME_PAGE_TYPE is set to 'Page'
    """
    template_name = 'pages/home.html'


@cache_control(must_revalidate=True, max_age=3600)
def blog_latest(request):
    """
    Gets the latest post(s) from Contentful
    """

    today = datetime.today()

    # How many posts to show
    if ENTRIES['BLOG']['BLOG_LATEST_NUMBER'] > 0:
        blog_latest_number = ENTRIES['BLOG']['BLOG_LATEST_NUMBER']
    elif ENTRIES['BLOG']['BLOG_LATEST_NUMBER'] == 0:
        # Max is 1000
        # https://www.contentful.com/developers/docs/references/content-delivery-api/#/reference/search-parameters/limit
        blog_latest_number = 1000
    else:
        raise ValueError("Config: 'BLOG_LATEST_NUMBER' must be an positive, round number.")

    # Get the posts from Contentful
    posts = client.entries(
            {'content_type': 'blogPost',                # Content Type
             'include': 3,                              # Number of links to fetch
             'limit': blog_latest_number,               # Limit number of posts, set in config
             'order': '-fields.publicatie_datum',       # Reverse order to get latest
             'fields.publicatie_datum[lte]': today,     # Only get published today or earlier
             }
        )

    context = {
        'posts': posts,
    }

    return render(request, 'blog/latest.html', context)


@cache_control(must_revalidate=True, max_age=3600)
def blog_archive(request, year=date.today().year):
    """
    View to show the (yearly) archive. Shows entries on a per year basis
    """

    year_min = '{}-01-01'.format(year)
    year_min = datetime.strptime(year_min, "%Y-%m-%d")
    year_max = '{}-12-31'.format(year)
    year_max = datetime.strptime(year_max, "%Y-%m-%d")

    current_year = year
    today = datetime.today()

    # First, get all posts
    posts = client.entries({
        'content_type': 'blogPost',
        'order': '-fields.publicatie_datum',
    })

    # Get all years with posts
    posts_years = []
    for post in posts:
        # Exclude published, but in future
        if post.publicatie_datum <= today:
            year = post.publicatie_datum.year
            posts_years.append(int(year))

    # Get unique values
    posts_years = list(set(posts_years))
    if len(posts_years) > 1:
        posts_years.sort(reverse=True)

    # Get posts from chosen year
    posts_current = [post for post in posts if year_min <= post.publicatie_datum <= year_max
                     and post.publicatie_datum <= today]

    context = {
        'posts': posts_current,
        'posts_years': posts_years,
        'current_year': current_year,
    }

    return render(request, 'blog/archive.html', context)


@cache_control(must_revalidate=True, max_age=3600)
def blog_single(request, slug):
    """
    Gets the single post
    """
    try:
        post = client.entries(
            {'content_type': 'blogPost',
             'fields.slug': slug,
             'include': 3, }
        )[0]    # [0] so we don't have to use a loop in the template
    except IndexError:
        raise Http404('No message found')

    context = {
        'post': post,
    }

    return render(request, 'blog/single.html', context)


class ContactView(FormView):
    template_name = 'contact/contact.html'
    success_url = reverse_lazy('thanks')
    form_class = ContactForm

    def form_valid(self, form):
        form.send_email()
        return super(ContactView, self).form_valid(form)


class ContactBedanktView(TemplateView):
    template_name = 'contact/thanks.html'
    build_path = 'contact/thanks.html'
