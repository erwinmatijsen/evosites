from django import forms
from django.core.mail import EmailMessage
from django.template.loader import get_template
from evosites.config import CONTACT_EMAIL, SITE_NAME


class ContactForm(forms.Form):
    name = forms.CharField(max_length=500, required=True,
                           widget=forms.TextInput(attrs={'class': 'input'}))
    email = forms.EmailField(label='E-mail', required=True,
                             widget=forms.EmailInput(attrs={'class': 'input'}))
    subject = forms.CharField(max_length=100, required=True,
                              widget=forms.TextInput(attrs={'class': 'input'}))
    message = forms.CharField(label='Bericht', required=True,
                              widget=forms.Textarea(attrs={'class': 'textarea', 'rows': '25'}))

    def send_email(self):
        name = self.cleaned_data['name']
        sender_email = self.cleaned_data['email']
        subject = '[Contactform ' + SITE_NAME + '] ' + self.cleaned_data['subject']
        subject_simple = self.cleaned_data['subject']
        message = self.cleaned_data['message']

        from_email = CONTACT_EMAIL
        to_email = [CONTACT_EMAIL, ]

        ctx = {
            'subject': subject_simple,
            'message': message,
            'sender_email': sender_email,
            'name': name,
            'site': SITE_NAME,
        }

        message = get_template('contact/email.html').render(ctx)
        msg = EmailMessage(subject, message, to=to_email, from_email=from_email)
        msg.content_subtype = 'html'
        msg.send()

