# Een eigen template toevoegen

## Maak de juiste folders aan
mkdir templates/[naam]  
mkdir templates/[naam]/templates  
mkdir templates/[naam]/static  
touch templates/[naam]/\__init__.py  


__Structuur__  

-evosites  
|----templates     
|--------\<templatenaam>  
|------------\__init\__.py  
|

________[naam]  
____________\__init\__.py    
____________ static/  
------------templates/  
----------------blog-latest.html  
------------templatetags/  
----------------\__init__.py    
----------------templatetags.py    

            
\_______


# Template tags and variables

## Blog

### Template: blog/latest.html
    {{ posts }}   

Example:  


    {% for post in posts %}  
        <h1>{{ post.title }}</h1>  
    {% endfor %}


### Template: blog/archive.html
    {{ current_year }}
    {{ posts }}
    {{ post_years }}
    
Example:

    <h1>All posts in {{ current_year }}</h1>
    {% for post in posts %}
        {{ post.title }}
    {% endfor %}
    
    <strong>Posts in other years</strong>
    <ul>
    {% for year in posts_years %}
        <li><a href="{% url 'blog-archive' year=year %}">{{ year }}</a></li>
    {% endfor %}
    </ul>
    
### Template: blog/single.html
    {{ post }}
    
Example:

    <h1>{{ post.title }}</h1>